<?php

use PharIo\Manifest\PhpElement;
use Sorani\Collections\Collection;

require __DIR__ . '/CollectionInterface.php';
require __DIR__ . '/Collection.php';

$data = [];
for ($i = 1; $i <= 10; $i++) {
    $post = new \stdClass;
    $post->id = $i;
    $post->title = 'My title ' . $i;
    $post->content = 'My content ' . $i;
    $post->createdAt = new DateTimeImmutable();
    $data['post' . $i] = $post;
}

$collection = new Collection($data);

var_dump($collection->get('post1')->title === 'My title 1', 'Title1');

var_dump($collection->get('post1000') === null, 'default null');
var_dump($collection->get('post1000', 'yoho') === 'yoho', 'default yoho');
var_dump($collection['post1'] instanceof \stdClass, '$collection[post1] instanceof \stdClass');

var_dump(count($collection) === 10, 'count');

echo PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . 'foreach test' . PHP_EOL;

foreach ($collection as $key => $value) {
    echo 'ID: ' . $key . ' ';
    var_export($value);
    echo PHP_EOL;
}
for ($i = 0; $i < count($collection); $i++) {
    var_export($collection['post' . $i]);
}
$collection['post10']->id = 100;
var_dump('$collection[post10]->offsetSet(id=100)', $collection['post10']);
var_dump('$collection[post10]->offsetExists', isset($collection['post10']));
unset($collection['post10']);
var_dump('$collection[post10]->offsetUnset', $collection['post10']);
var_dump('$collection[post10]->offsetExists', isset($collection['post10']));

$post = new \stdClass;
$post->id = 100;
$post->title = 'My title ' . 1;
$post->content = 'My content ' . 1;
$post->createdAt = new DateTimeImmutable();
var_dump('$collection[post1]->set(id=100)', $collection->set('post1', $post));
var_dump('$collection[post1]->get the id', $collection->get('post1')->id);
var_dump('$collection[post1]->has', $collection->has('post1'));
unset($collection['post1']);
var_dump('$collection[post1]->remove', $collection->remove('post1'));
var_dump('$collection[post1]->has', $collection->has('post1'));

var_dump($collection->asList('id', 'title'));
var_dump($collection->extract('id'));

// var_dump('implode', $collection->join(), $collection->join(PHP_EOL));
var_dump($collection->extract('id')->join());
var_dump($collection->extract('id')->join('----------' . PHP_EOL));


var_dump('max', $collection->max(), $collection->max('id'), $collection->max('content'));

echo PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;

echo "=========================================";

echo PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;

$data = [];
for ($i = 1; $i <= 10; $i++) {
    $post = [];
    $post["id"] = $i;
    $post["title"] = 'My title ' . $i;
    $post["content"] = 'My content ' . $i;
    $post["createdAt"] = new DateTimeImmutable();
    $post['author'] = [
        'name' => 'Sarah Connor',
        'address' => [
            'city' => 'Hunstville',
            'zip' => 10000,
            'street' => '123, main st',
        ],
        'contact' => [
            'email@fafaf.gz',
            '005151221'
        ]
    ];
    $data['post' . $i] = $post;
}
$collection = new Collection($data);

var_dump($collection->get('post1')->title === 'My title 1', 'Title1');

var_dump($collection->get('post1000') === null, 'default null');
var_dump($collection->get('post1000', 'yoho') === 'yoho', 'default yoho');
var_dump($collection['post1'] instanceof \stdClass, '$collection[post1] instanceof \stdClass');

var_dump(count($collection) === 10, 'count');

echo PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . PHP_EOL . 'foreach test' . PHP_EOL;

foreach ($collection as $key => $value) {
    echo 'ID: ' . $key . ' ';
    var_export($value);
    echo PHP_EOL;
}
for ($i = 0; $i < count($collection); $i++) {
    var_export($collection['post' . $i]);
}
$collection['post10']['id'] = 100;
var_dump('$collection[post10]->offsetSet(id=100)', $collection['post10']);
var_dump('$collection[post10]->offsetExists', isset($collection['post10']));
unset($collection['post10']);
var_dump('$collection[post10]->offsetUnset', $collection['post10']);
var_dump('$collection[post10]->offsetExists', isset($collection['post10']));

$post = [];
$post['id'] = 100;
$post['title'] = 'My title ' . 1;
$post['content'] = 'My content ' . 1;
$post['createdAt'] = new DateTimeImmutable();
var_dump('$collection[post1]->set(id=100)', $collection->set('post1', $post));
var_dump('$collection[post1]->get the id', $collection->get('post1')['id']);
var_dump('$collection[post1]->has', $collection->has('post1'));
unset($collection['post1']);
var_dump('$collection[post1]->remove', $collection->remove('post1'));
var_dump('$collection[post1]->has', $collection->has('post1'));

var_dump($collection->asList('id', 'title'));
var_dump($collection->extract('id'));

// var_dump('implode', $collection->join(), $collection->join(PHP_EOL));
var_dump($collection->extract('id')->join());
var_dump($collection->extract('id')->join('----------' . PHP_EOL));


var_dump('max', $collection->max(), $collection->max('id'), $collection->max('content'));

echo PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;
echo PHP_EOL;

var_dump($collection->get('post5.id'));
var_dump($collection->get('post5.id'));
var_dump($collection->get('post5.address'));
var_dump($collection->get('post5.author.address.city'));
var_dump($collection->get('post5.author.contact'));
var_dump($collection->get('post5.author.contact.0'));
var_dump($collection->get('post5.author.contact.1'));

echo PHP_EOL;
var_dump($collection->get('post5.author.contact')->get('0'));