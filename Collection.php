<?php

namespace Sorani\Collections;

use ArrayIterator;

/**
 * {@inheritdoc}
 */
class Collection implements CollectionInterface, \ArrayAccess, \IteratorAggregate, \Countable
{
    /**
     * @var array
     */
    private $items;

    /**
     * Collection Constructor
     *
     * @param  array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * {@inheritdoc}
     */
    public function get($key, $default = null)
    {
        if (false === strpos($key, '.')) {
            return $this->has($key) ? $this->items[$key] : $default;
        }
        $index = explode('.', $key);
        return $this->getValue($index, $this->items, $default);
    }


    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        return isset($this->items[$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        $this->items[$key] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function remove($key)
    {
        if ($this->has($key)) {
            unset($this->items[$key]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function all()
    {
        return new self($this->items);
    }

    /**
     * {@inheritdoc}
     */
    public function asArray()
    {
        return $this->items;
    }

    /**
     * {@inheritdoc}
     */
    public function asList($key, $value)
    {
        $results = [];
        foreach ($this->items as $item) {
            if (is_object($item)) {
                $results[$item->$key] = $item->$value;    
            } else {
                $results[$item[$key]] = $item[$value];
            }
        }
        return new self($results);
    }

    /**
     * {@inheritdoc}
     */
    public function extract($key)
    {
        if (version_compare(PHP_VERSION, '5.5.0', '>=')) {
            return new self(array_column($this->items, $key, $key));
        } else {
            $results = [];
            foreach ($this->items as $item) {
                if (is_object($item)) {
                    $results[] = $item->$key;
                } else {
                    $results[] = $item[$key];
                }
            }
            return new self($results);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function join($glue = ',')
    {
        return implode($glue, $this->items);
    }


    /**
     * {@inheritdoc}
     */
    public function max($key = null)
    {
        if (null === $key) {
            return max($this->items);
        }
        return $this->extract($key)->max();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset,  $value)
    {
        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
        // return (function() {
        //     $cnt = count($this->items);
        //     foreach($this->items as $key => $value) {
        //         yield $key => $value;
        //     }
        // })();
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return count($this->items);
    }

    
    /**
     * Get a value from the key by dot notation given
     *
     * @param  array $indexes List of indexes having been exploded by the get method from key1.key2
     * @param  mixed $value the current value in the items
     * @param  mixed|null $default the default value
     * @return mixed|CollectionInterface
     */
    private function getValue($indexes, $value, $default = null)
    {
        $key = array_shift($indexes);
        // var_dump(func_get_args(), $indexes, $key);
        if (empty($indexes)) {
            // var_dump(__LINE__, $value, $value[$key]);
            if (!isset($value[$key])) {
                return $default;
            }
            if (is_array($value[$key])) {
                return new self($value[$key]);
            }
            return $value[$key];
        }
        return $this->getValue($indexes, $value[$key], $default);
    }
}
