<?php

namespace Sorani\Collections;

/**
 * @package Sorani\Collections
 * Interface CollectionInterface
 */
interface CollectionInterface
{
	/**
	 * Retrieve an item from the Collection or $default if none found
	 * @param  string     $key Item to find can be given as the key itself or as a dot notation (e.g.: key1.key11.key112) traversing the array
	 * @return mixed|null $default Default value, null by default
	 */
	public function get($key, $default = null);

	/**
	 * Check if an item exists
	 * @param  string Item key
	 * @return bool
	 */
	public function has($key);

	/**
	 * set an item
	 *
	 * @param  string $key Item key
	 * @param  mixed  $value Value to store
	 * @return void
	 */
	public function set($key, $value);

	/**
	 * Remove an item from the Collection
	 * 
	 * @param string
	 * @param string $key Item key
	 * @return void
	 */
	public function remove($key);

	/**
	 * Get all values of the Collection
	 * 
	 * @return Collection
	 */
	public function all();

	/**
	 * Get all values of the Collection as an array
	 * 
	 * @return array
	 */
	public function asArray();
	
	/* Retrieve a list of values as a key value pair from the Collection as a new Collection
	 * [key1 => value1, key2 => value2]
	 * iterates over the items and returns it as a list
	 * 
	 * @param string $key Returned array key
	 * @param string $value Returned array value
	 * @return CollectionInterface	 
	 */
	public function asList($key, $value);

	/**
	 * Get a column from the Collection as a new Collection
	 * @param string $key  Column name
	 * @return CollectionInterface
	 */
	public function extract($key);

	/**
	 * Get the items as a string tried togather by a $glue
	 * @param string $glue  Glue 
	 */
	public function join($glue = ',');

	/**
	 * Get the maximum
	 * @param string $key Key to search for. If null search the entire Collection
	 * @return mixed
	 */
	public function max($key = null);

}
